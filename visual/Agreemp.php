<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>CINITED</title>
	</head>
	<body>
		<!-- SCRIPTS para la validación de las cajas de texto para evitar caracteres que no son permitidos-->
		<script>
			function soloLetras(e) {
				var key = e.keyCode || e.which,
				tecla = String.fromCharCode(key).toLowerCase(),
				letras = " asdfghjklzxcvbnmqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM",
				tecla_especial = false
				
				if (letras.indexOf(tecla) == -1 ) {
					return false;
				}
			}
		</script>
		<script>
			function soloLetrasNumero(e) {
				var key = e.keyCode || e.which,
				tecla = String.fromCharCode(key).toLowerCase(),
				letras = " asdfghjklzxcvbnmqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM1234567890-",
				tecla_especial = false
				
				if (letras.indexOf(tecla) == -1 ) {
					return false;
				}
			}
		</script>
		<!-- Formulario con medoto post para realizar el registro de los empleados-->
		<form id="form1" name="form1" method="post">
			<!-- Creacion de tabla para tener un orden con las cajas de textos, botones y texto que se van a visualizar en la pagina -->
			<table width="" border="1" align="center">
				<tbody>
					<tr>
						<!-- Creacion de cada una de las cajas de texto-->
						<td><p>Primer apellido</p></td>
						<td><input name="pape_emp" type="text" id="textfield" placeholder="Solo letras en Mayuscula" onkeypress="return soloLetras(event)"  pattern="[A-Z ]+" maxlength="20" ></td>
					</tr>
					<tr>
						<td><p>Segundo apellido</p></td>
						<td><input name="sape_emp" type="text" id="textfield2"  placeholder="Solo letras en Mayuscula" maxlength="20" onkeypress="return soloLetras(event)" pattern="[A-Z ]+"></td>
					</tr>
					<tr>
						<td><p>Primer nombre</p></td>
						<td><input name="pnom_emp" type="text" id="textfield3"  placeholder="Solo letras en Mayuscula"   maxlength="20" onkeypress="return soloLetras(event)" pattern="[A-Z ]+"></td>
					</tr>
					<tr>
						<td><p>Otros nombre</p></td>
						<td><input name="snom_emp" type="text" id="textfield4"  placeholder="Solo letras en Mayuscula"   maxlength="50" onkeypress="return soloLetras(event)" pattern="[A-Z ]+"></td>
					</tr>
					<tr>
						<!--Creacion de un listado o combo de texto para que el usuario seleccione el pais al que pertenece el emplado -->
						<td><p>Pais</p></td>
						<td><select name="pais_emp" title="">
							<option value="">Seleccione</option>
							<option value="co">Colombia</option>
							<option value="us">Estados unidos</option> 
							
						</select></td>
					</tr>
					<tr>
						<!--Creacion de un listado o combo de texto para que el usuario seleccione el tipo de documento que tiene el emplado -->
						<td><p>Tipo de documento</p></td>
						<td><p>
							<select name="tipnit_emp" title="">
								<option value="">Seleccione</option>
								<option value="CC">Cédula de Ciudadanía</option>
								<option value="CE">Cédula de Extranjería</option>
								<option value="P">Pasaporte</option>
								<option value="PE">Permiso Especial</option>
							</select>
							<br>
						</p></td>
					</tr>
					<tr>
						<!--Creacion de caja de texto el numero de identificacion -->
						<td><p>Número de Identificación</p></td>
						<td><input name="nit_emp" type="text" id="textfield5"  placeholder="Mayuscula y Numeros"  maxlength="20" onkeypress="return soloLetrasNumero(event)" pattern="[A-Z-0-9]+"></td>
					</tr>
					<tr>
						<!-- Creacion  de un date que va a permitir por medio de codigo php un rango minino que es igual a la fecha actual de hoy menos un mes y como maximo la fecha actual de hoy-->
						<td><p>Fecha de ingreso</p></td>
						<td><input name= "fecing_emp" type="date" min="<?php
							//Variable para captar la fecha de hoy
							$fecha_actual = date("d-m-Y");
							//se imprime la fecha minima que es del dia actual menos un mes para que el date lo tome como fecha minima que se va a tomar
							echo date("Y-m-d", strtotime($fecha_actual."- 1 month"))
							?>" max="<?php
							//Se imprime en pantalla la fecha de hoy para que el date solo permita esta fecha como maxima
							echo date("Y-m-d")
						?>"></td>
					</tr>
					<tr>
						<!--Creacion de un listado o combo de texto para seleccionar el area al cual pertenece el empleado -->
						<td><p>Area</p></td>
						<td><select name="area_emp" title="">
							<option value="">Seleccione</option>
							<option value="Administracion">Administración</option>
							<option value="Financiera">Financiera</option>
							<option value="Compras">Compras</option>
							<option value="Operacion">Operación</option>
							<option value="Talento Humano">Talento Humano</option>
							<option value="Servicios Varios">Servicios Varios</option>		  
						</select></td>
					</tr>
					<tr>
						<!-- Aqui se muestra el estado con el cual va a ingresar el empleado pero simpre sera estatico como ACTIVO -->
						<td><p>Estado</p></td>
						<td><label>ACTIVO</label></td>
					</tr>
					<tr>
						<!-- Aqui se muestra la fecha actual con horario zonal de Bogotá, Colombia el cual se va a registrar en la base de datos de que dia se a realizado el registro del empleado-->
						<td><p>Fecha de registro</p></td>
						<td><?php 
							date_default_timezone_set('America/Bogota');
						echo date("Y-m-d H:i:s");?></td>
					</tr>
					
					<tr>
						<td><p>
							<!-- Creacion de botones el primero para agregar el empleado el segundo para cargar nuevamente la pagina y un hiper vinculo que nos permite regresar al menu de opciones-->
							<input type="submit" name="agremp" id="submit" value="Agregar empleado">
						</p></td>
						<td><input type="reset" name="reset" id="reset" value="Restablecer">
						<a href="../index.php"> Regresar</a></td>
					</tr>
				</tbody>
			</table>
			
			<?php
				
				//Validacion para verificar que se preciono el bonto para agregar empleado
				if (isset($_REQUEST['agremp']))
				{
					//asignacion de variables para cada campo de los datos llenados en el formulario
					$pape_emp = $_REQUEST['pape_emp'];
					$sape_emp = $_REQUEST['sape_emp'];
					$pnom_emp = $_REQUEST['pnom_emp'];
					$snom_emp = $_REQUEST['snom_emp'];
					$pais_emp = $_REQUEST['pais_emp'];
					$tipnit_emp = $_REQUEST['tipnit_emp'];
					$nit_emp = $_REQUEST['nit_emp'];
					$fecing_emp =  $_REQUEST['fecing_emp'];
					$area_emp = $_REQUEST['area_emp'];
					$est_emp = "ACTIVO";
					$fecreg_reemp = date("Y-m-d H:i:s");
					//validacion que ningun campo este en blanco o no haya dejado sin seleccionar un listado
					if (trim($pape_emp) == '' || trim($sape_emp) == '' || trim($pnom_emp) == '' || trim($pais_emp) == '' || trim($tipnit_emp) == '' || trim($nit_emp) == '' || trim($snom_emp) == '' || trim($fecing_emp ) == '' || $area_emp == '' )
					{		
						//En caso de estar uno campo en blanco nos enviara un mensaje por medio de script alertando que falta un campo
						echo "<script>
						alert('Todos los campos son necesarios para el registro');
						window.location= 'agreemp.php'
						</script>";
					}
					else
					{	
						//incluimos a empleados. php que pertenece a la capa de funcionalidad, en la cual estan guardados todos los procesos previos antes de la conexion a la base de datos
						include '../datos/Empleados.php';	
						
						//Llamamos a la funcion insertar_empleado y le enviamos todos los valores previamente asignados a las variables
						insertar_empleado($pape_emp,$sape_emp,$pnom_emp,$snom_emp,$pais_emp,$tipnit_emp,$nit_emp,$fecing_emp,$area_emp,$est_emp,$fecreg_reemp);
						
					}
				}
			?>
		</form>
	</body>
</html>
