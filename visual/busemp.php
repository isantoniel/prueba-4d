<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>CIDENET</title>
		<!--Se importa la hoja de estilo para la tabla interactiva -->
		<link rel="stylesheet"  href="../css/jquery.dataTables.min.css" >
	</head>
	
	<body>
		
		
		
		<?php 
		//Incluimos el archivo empleados.php que pertenece a la capa de funcionanlidad alojado en la carpeta datos
			include'../datos/Empleados.php';
		//Asignamos a la variable id el valor de que tipo de filtro sea utilizado 
			$id = $_GET['id'];
		?>
		<!-- Utilizamos el script soloLetras para validar los caracteres permitidos en la caja des texto -->
		<script>
			function soloLetras(e) {
				var key = e.keyCode || e.which,
				tecla = String.fromCharCode(key).toLowerCase(),
				letras = " asdfghjklzxcvbnmqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM",
				tecla_especial = false
				
				if (letras.indexOf(tecla) == -1 ) {
					return false;
				}
			}			
		</script>
		<!-- Utilizamos el script soloLetras para validar los caracteres permitidos en la caja des texto -->
		<script>
			function soloLetrasNumero(e) {
				var key = e.keyCode || e.which,
				tecla = String.fromCharCode(key).toLowerCase(),
				letras = " asdfghjklzxcvbnmqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM123456789-.@",
				tecla_especial = false
				
				if (letras.indexOf(tecla) == -1 ) {
					return false;
				}
			}
		</script>
		<div>
			<!--Creacion de la una tabla que contener todas las demas tablas en las cuales estan los filtros y botonoces de acciones --> 
			<table width="" border="0" align="center">
				<tbody>
					<tr>
						<!-- Creacion de tabla que va a dar un mejor orden a las siguentes formularios con sus respectivas tablas-->
						<td><table width="" border="0" align="center">
							<tbody>
								<tr>
									<td>
										<!--Formulario que contine una caja de texto para hacer el filtro por primer apellido y un boton para accionar la busqueda-->
										<form action="busemp.php?id=2"  method="post">
										<table width="" border="1">
											<tbody>
												<tr>
													<td>Primer apellido</td>
												</tr>
												<tr>
													<td><input name="pape_emp" type="text" id="textfield" placeholder="Solo letras en Mayuscula" onkeypress="return soloLetras(event)"  pattern="[A-Z ]+" maxlength="20" ></td>
												</tr>
												<tr>
													<td><input type="submit" name="con_pape_emp" id="submit" value="Filtar" ></td>
												</tr>
											</tbody>
										</table>
									</form></td>
									<td>
										<!--Formulario que contine una caja de texto para hacer el filtro por Segundo apellido y un boton para accionar la busqueda-->
										<form action="busemp.php?id=3"  method="post">
										<table width="" border="1">
											<tbody>
												<tr>
													<td>Segundo apellido</td>
												</tr>
												<tr>
													<td><input name="sape_emp" type="text" id="textfield2"  placeholder="Solo letras en Mayuscula" maxlength="20" onkeypress="return soloLetras(event)" pattern="[A-Z ]+"></td>
												</tr>
												<tr>
													<td><input type="submit" name="con_sape_emp" id="submit2" value="Filtar"></td>
												</tr>
											</tbody>
										</table>
									</form></td>
									<td>
										<!--Formulario que contine una caja de texto para hacer el filtro por Primer nombre y un boton para accionar la busqueda-->
										<form action="busemp.php?id=4"  method="post">
										<table width="" border="1">
											<tbody>
												<tr>
													<td>Primer nombre</td>
												</tr>
												<tr>
													<td><input name="pnom_emp" type="text" id="textfield3"  placeholder="Solo letras en Mayuscula"   maxlength="20" onkeypress="return soloLetras(event)" pattern="[A-Z ]+"></td>
												</tr>
												<tr>
													<td><input type="submit" name="con_pnom_emp" id="submit3" value="Filtar"></td>
												</tr>
											</tbody>
										</table>
									</form>
									</td>
									<td>
										<!--Formulario que contine una caja de texto para hacer el filtro por Otros nombre y un boton para accionar la busqueda-->
										<form action="busemp.php?id=5"  method="post">
											<table width="" border="1">
												<tbody>
													<tr>
														<td>Otros nombre</td>
													</tr>
													<tr>
														<td><input name="snom_emp" type="text" id="textfield4"  placeholder="Solo letras en Mayuscula"   maxlength="50" onkeypress="return soloLetras(event)" pattern="[A-Z ]+"></td>
													</tr>
													<tr>
														<td><input type="submit" name="con_snom_emp" id="submit4" value="Filtar"></td>
													</tr>
												</tbody>
											</table>
										</form>
									</td>
									<td>
										<!--Formulario que contine un combo de texto para hacer el filtro por Pais y un boton para accionar la busqueda-->
										<form action="busemp.php?id=6"  method="post">
											<table width="" border="1">
												<tbody>
													<tr>
														<td>Pais</td>
													</tr>
													<tr>
														<td><select name="pais_emp" title="">
															<option value="">Seleccione</option>
															<option value="co">Colombia</option>
															<option value="us">Estados unidos</option>
														</select></td>
													</tr>
													<tr>
														<td><input type="submit" name="con_pais_emp" id="submit5" value="Filtar"></td>
													</tr>
												</tbody>
											</table>
										</form>
									</td>
									
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td><table width="" border="0" align="center">
							<tbody>
								<tr>
									<td>
										<!--Formulario que contine un combo de texto para hacer el filtro por Tipo de documento y un boton para accionar la busqueda-->
										<form action="busemp.php?id=7"  method="post">
											<table width="" border="1">
												<tbody>
													<tr>
														<td>Tipo de documento</td>
													</tr>
													<tr>
														<td><select name="tipnit_emp" title="">
															<option value="">Seleccione</option>
															<option value="CC">Cédula de Ciudadanía</option>
															<option value="CE">Cédula de Extranjería</option>
															<option value="P">Pasaporte</option>
															<option value="PE">Permiso Especial</option>
														</select></td>
													</tr>
													<tr>
														<td><input type="submit" name="con_tipnit_emp" id="submit6" value="Filtar"></td>
													</tr>
												</tbody>
											</table>
										</form>
									</td>
									<td>
										<!--Formulario que contine una caja de texto para hacer el filtro por Número de Identificación y un boton para accionar la busqueda-->
										<form action="busemp.php?id=8"  method="post">
											<table border="1">
												<tbody>
													<tr>
														<td>Número de Identificación</td>
													</tr>
													<tr>
														<td><input name="nit_emp" type="text" id="textfield5"  placeholder="Mayuscula y Numeros"  maxlength="20" onkeypress="return soloLetrasNumero(event)" pattern="[A-Z-0-9]+"></td>
													</tr>
													<tr>
														<td><input type="submit" name="con_nit_emp" id="submit7" value="Filtar"></td>
													</tr>
												</tbody>
											</table>
										</form>
									</td>
									<td>
										<!--Formulario que contine un combo de texto para hacer el filtro por Estado y un boton para accionar la busqueda-->
										<form action="busemp.php?id=9"  method="post">
											<table border="1">
												<tbody>
													<tr>
														<td>Estado</td>
													</tr>
													<tr>
														<td><select name="est_emp" title="">
															<option value="">Seleccione</option>
															<option value="ACTIVO">ACTIVO</option>
															<option value="INACTIVO">INACTIVO</option>
															
														</select></td>
													</tr>
													<tr>
														<td><input type="submit" name="con_est_emp" id="submit8" value="Filtar"></td>
													</tr>
												</tbody>
											</table>
										</form>
									</td>
									<td>
										<!--Formulario que contine una caja de texto para hacer el filtro por Correo y un boton para accionar la busqueda-->
										<form action="busemp.php?id=10"  method="post">
											<table border="1" align="center">
												<tbody>
													<tr>
														<td>Correo</td>
													</tr>
													<tr>
														<td><input name="cor_emp" type="text" id="textfield6"  placeholder="Mayuscula y Numeros"  maxlength="300" onkeypress="return soloLetrasNumero(event)" ></td>
													</tr>
													<tr>
														<td><input type="submit" name="con_cor_emp" id="submit9" value="Filtar"></td>
													</tr>
												</tbody>
											</table>
										</form>
									</td>
								</tr>
							</tbody>
						</table></td>
					</tr>
					<tr>
						<td>
							<!--Formulario que contine botones para accionar la busqueda de todos los registros y un hiper vinculo para regresar al menu principal-->
							<form action="busemp.php?id=1"  method="post">
								<table border="1" align="center">
									<tbody>
										<tr>
											<td><input type="submit" name="con_mostod_emp" id="submit10" value="Mostrar todo"></td>              
											<td><a href="../index.php"> Regresar</a></td>
										</tr>
									</tbody>
								</table>
							</form>	  
						</a>
					</td>
				</tr>
				<tr>
					<td>
						<class>
							
						</class>
					</td>
				</tr>
			</tbody>
		</table>
		
		
		<?php 

			//Este if nos va validar que busqueda vamos a realizar dependiendo que filtro se aplico iniciando con el codigo 1 que es para mostrar todos registros y terminando con el codigo 10 que es para filtrar por correos
			if ($id == 1)
			{
				//llamamos la funcion con_emp que premite consultar todos los registros esta alojada en Empleados.php
				$resultado = con_emp();
			}
			//consultar por Primer apellido
			elseif ($id == 2)
			{
				//Asignamos a la variable buscar lo que ha ingresado el usuario en la caja de texto
				$buscar = $_POST['pape_emp'];		
				//En la variable conducion le daremos el condicional para la consulta en la base de datos por merdio de MySQL
				$condicion = " pape_emp ='".$buscar."'";
				//Llamamos a la funcion conava_emp la cual le enviamos el condicional de consulta y este nos va a retornar la consulta con la cual vamos a generar la tabla 
				$resultado = conava_emp($condicion);
				
			}
			//Consulta por Segundo Apellido
			elseif ($id == 3)
			{
				//Asignamos a la variable buscar lo que ha ingresado el usuario en la caja de texto
				$buscar = $_POST['sape_emp'];		
				//En la variable conducion le daremos el condicional para la consulta en la base de datos por merdio de MySQL
				$condicion = " sape_emp ='".$buscar."'";
				//Llamamos a la funcion conava_emp la cual le enviamos el condicional de consulta y este nos va a retornar la consulta con la cual vamos a generar la tabla 
				$resultado = conava_emp($condicion);
				
			}
			//Consulta por Primer Nombre
			elseif ($id == 4)
			{
				//Asignamos a la variable buscar lo que ha ingresado el usuario en la caja de texto
				$buscar = $_POST['pnom_emp'];		
				//En la variable conducion le daremos el condicional para la consulta en la base de datos por merdio de MySQL
				$condicion = " pnom_emp ='".$buscar."'";
				//Llamamos a la funcion conava_emp la cual le enviamos el condicional de consulta y este nos va a retornar la consulta con la cual vamos a generar la tabla 
				$resultado = conava_emp($condicion);
				
			}
			//Consulta por Segundo Nombre
			elseif ($id == 5)
			{
				//Asignamos a la variable buscar lo que ha ingresado el usuario en la caja de texto
				$buscar = $_POST['snom_emp'];		
				//En la variable conducion le daremos el condicional para la consulta en la base de datos por merdio de MySQL
				$condicion = " snom_emp ='".$buscar."'";
				//Llamamos a la funcion conava_emp la cual le enviamos el condicional de consulta y este nos va a retornar la consulta con la cual vamos a generar la tabla 
				$resultado = conava_emp($condicion);
				
			}
			//Consulta por Pais
			elseif ($id == 6)
			{
				//Asignamos a la variable buscar lo que ha ingresado el usuario en la caja de texto
				$buscar = $_POST['pais_emp'];	
				//En la variable conducion le daremos el condicional para la consulta en la base de datos por merdio de MySQL
				$condicion = " pais_emp ='".$buscar."'";
				//Llamamos a la funcion conava_emp la cual le enviamos el condicional de consulta y este nos va a retornar la consulta con la cual vamos a generar la tabla 
				$resultado = conava_emp($condicion);
				
			}
			//Consulta por Tipo de Documento de identificacion
			elseif ($id == 7)
			{
				//Asignamos a la variable buscar lo que ha ingresado el usuario en la caja de texto
				$buscar = $_POST['tipnit_emp'];	
				//En la variable conducion le daremos el condicional para la consulta en la base de datos por merdio de MySQL
				$condicion = " tipnit_emp ='".$buscar."'";
				//Llamamos a la funcion conava_emp la cual le enviamos el condicional de consulta y este nos va a retornar la consulta con la cual vamos a generar la tabla 
				$resultado = conava_emp($condicion);
				
			}
			//Consulta por Documento de identificacion
			elseif ($id == 8)
			{
				//Asignamos a la variable buscar lo que ha ingresado el usuario en la caja de texto
				$buscar = $_POST['nit_emp'];
				//En la variable conducion le daremos el condicional para la consulta en la base de datos por merdio de MySQL
				$condicion = " nit_emp ='".$buscar."'";
				//Llamamos a la funcion conava_emp la cual le enviamos el condicional de consulta y este nos va a retornar la consulta con la cual vamos a generar la tabla 
				$resultado = conava_emp($condicion);
				
			}
			//Consulta por Estado
			elseif ($id == 9)
			{
				//Asignamos a la variable buscar lo que ha ingresado el usuario en la caja de texto
				$buscar = $_POST['est_emp'];	
				//En la variable conducion le daremos el condicional para la consulta en la base de datos por merdio de MySQL
				$condicion = " est_emp ='".$buscar."'";
				//Llamamos a la funcion conava_emp la cual le enviamos el condicional de consulta y este nos va a retornar la consulta con la cual vamos a generar la tabla 
				$resultado = conava_emp($condicion);
				
			}
			//Consulta por Correo
			elseif ($id == 10)
			{
				//Asignamos a la variable buscar lo que ha ingresado el usuario en la caja de texto
				$buscar = $_POST['cor_emp'];	
				//En la variable conducion le daremos el condicional para la consulta en la base de datos por merdio de MySQL
				$condicion = " cor_emp ='".$buscar."'";
				//Llamamos a la funcion conava_emp la cual le enviamos el condicional de consulta y este nos va a retornar la consulta con la cual vamos a generar la tabla 
				$resultado = conava_emp($condicion);
				
			}
			
		?>
		
	</div>
	<!-- Importar la hoja de estilo para la tabla -->
	<link rel="stylesheet" type="text/css" href="../css/datatables.min.css"/>
	<!-- Importar el codigo de jquery para poder utilizar un plugin para poder hacer busqueda en tiempo real y ordenar la tabla y paginar la tabla-->
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<!-- Importar el plugin mensionado anteriormente-->
	<script type="text/javascript" src="../js/datatables.min.js"></script>
	<!-- Por medio de este script vamos a autilizar el plugin -->
	<script>
		//Vamos a inicar que cuando lea el documento realize la funcion 
		$(document).ready(function() {
		//Cuando lea la tabla llamada Tablamostrar vamos a aplicar el plugin
		$('#tablamostrar').DataTable(			
		{
		//Aqui se va a traducir el plugin a español
		"language":{
		"lengthMenu": "Mostrar _MENU_ registros por pagina",
		"info": "Mostrando pagina _PAGE_ de _PAGES_",
		"infoEmpty": "No hay registros disponibles",
		"infofiltered": "(filtrada de _MAX_ registros)",
		"loadingRecord": "Cargando...",
		"processing": "Procesando...",
		"search": "Buscar:",
		"zeroRecords": "No se encontraron registros coincidentes",
		"paginate": {
		"next": "Siguiente",
		"previous": "Anterior"
		},
		}
		}
		);
		} );
	</script>
	<!--Se crea la tabla con la cual se va a mostrar todos los resultados de los filtros y la consulta de todos los registros y se va a ser unidad al plugin del jquery -->
	<table id="tablamostrar" class="display" style="width:100%" border="1">
		<thead>
			<tr>
				<!--El encabezado de cada columna -->
				<th>Primer nombre</th>
				<th>Otros nombre</th>
				<th>Primer apellido</th> 
				<th>Segundo apellido</th>
				<th>Tipo de documento</th>
				<th>Numero de documento</th>
				<th>Pais</th>
				<th>Correo Elestronico</th>
				<Th>Estado</Th>
				<Th>Modificar</Th>
				<Th>eliminar</Th>
			</tr>
		</thead>
		<tbody>
			<!--Aqui por medio de php se van a mostrar cada una de las filas de la consulta realizada-->
			<!--Se va a usar la consulta realiza anterior mente en el if con el que se valido que tipo de consulta se habia realizado y con ayuda de un while vamos a recorrer cada fila que esta en esta tabla-->
			<?php while($row = $resultado->fetch_assoc()) {?>
				<tr	>
					<!--Aqui se va ir mostrando cada campo utilizando la variable row que contiene la informacion de cada fila segun vaya recoriendo el while-->
					<td><?php echo $row['pnom_emp']?></td>
					<td><?php echo $row['snom_emp']?></td>
					<td><?php echo $row['pape_emp']?></td>
					<td><?php echo $row['sape_emp']?></td>
					<!--Ya que al agregar a la base de datos se envia con un simbolo como ejemplo CC, P, PE se usa esta funcion-->
					<!--para que retorne el valor del codigo como ejempl si es cc retorna Cedula de Ciudadania-->
					<td><?php echo converti($row['tipnit_emp'])?></td>
					<td><?php echo $row['nit_emp']?></td>
					<!--Igualmente aqui se envia un simbolo a la base de datos para crear el correo us si es colombia o co si es colombia entonces por medio de la funcion converti nos retornara el pais sin el codigo o abreviación-->
					<td><?php echo converti($row['pais_emp'])?></td>
					<td><?php echo $row['cor_emp']?></td>
					<td><?php echo $row['est_emp']?></td>
					<!-- Estas dos columnas va a ser los botones que nos van a permitir modificar el empleado o eliminarlo de la base de datos-->
					<td><a href="../visual/Modemp.php?id=<?php echo $row['id_emp']?>"><button>Modificar</button></a></td>
					<td><a href="../visual/Eliemp.php?id=<?php echo $row['id_emp']?>"><button>Eliminar</button></a></td>
				</tr>
			<?php }?>
		</tbody>		
	</table>
<!--Autor: Israel Antonio Castiblanco Castiblanco 6-01-2021-->
</body>
</html>
