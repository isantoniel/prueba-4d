<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Documento sin título</title>
	</head>
	<body>
		<!--Este formulario contiene dos botones con los cuales vamos a confirmar o cancelar el borrado de un empleado-->
		<form id="form1" name="form1" method="post">
			<table width="" border="0" align="center">
				<tbody>
					<tr>
						<td><h2>¿Deseas continuar?</h2></td>
					</tr>
					<tr>
						<td>
							<table border="0">
								<tbody>
									<tr>
										<td><input type="submit" name="si" id="submit" value="Aceptar"></td>
										<td><input type="submit" name="no" id="reset" value="Cancelar" ></td>
									</tr>
								</tbody>
							</table></td>
					</tr>
				</tbody>
			</table>
			
			<?php
				//importamos a Empleados.php que es donde esta almacenado todas las funciones 
				include'../datos/Empleados.php';
				//Captamos el id del empleado que va a ser eliminado
				$id_emp = $_GET['id'];	
				//verificar si se a accionado un boton para condirmar con cancelar
				if (isset($_REQUEST['si']))
				{
					//Se llama la funcion eliemp y enviarmos el id del empleado para eliminalo
					eliemp ($id_emp);
					
				}
				elseif (isset($_REQUEST['no']))
				{
					//En casi de ser canselado volvemos nuevamente 
					header('Location: busemp.php?id=1');
				}
			?>
		</form>
		<!-- Autor: Israel Antonio Castiblanco Castiblanco 6-01-2021 -->
	</body>
</html>
