<?php
	//se inclue el archivo conexion.php que esta en la carpeta de conexion que es nuestra capa de conectividad
	include'../conexion/conexion.php';
	
	//funcion para captar los datos y realizar la insercion en la base de datos
	function insertar_empleado ($pape_emp,$sape_emp,$pnom_emp,$snom_emp,$pais_emp,$tipnit_emp,$nit_emp,$fecing_emp,$area_emp,$est_emp,$fecreg_reemp)
	{
		
		//llamado a la funcion que va ha verificar si este empleado se ha registrado antes
		$veri = veri_emp($tipnit_emp,$nit_emp);
		//if de si es nuevo o no el empleado
		if($veri == 1)
		{		
			//llamado a la funcion que permite la asignacion de correo
			$cor_emp = correo($pape_emp,$pnom_emp,$pais_emp);
			//realizar el guardado del empleado en la base de datos
			$sqle = "insert into empleado (tipnit_emp , nit_emp , pape_emp , sape_emp , pnom_emp , snom_emp , pais_emp , cor_emp , fecing_emp , area_emp , est_emp ) values ('".$tipnit_emp."','".$nit_emp."','" .$pape_emp."','".$sape_emp."','".$pnom_emp."','".$snom_emp."','".$pais_emp.
			"','".$cor_emp."','".$fecing_emp."','".$area_emp."','".$est_emp."')";	
			//variable que envia un mensaje adicional para mostrar el correo
			$mensaje = ", el correo del empleado es: ".$cor_emp;
			//Asigno el valor a la variable que dira que tipo de detalle de registro es aqui siempre seran registrado 
			$tip_regemp = "Registrado";
			//llama la funcion que va a realizar la insercion del empleado nuevo y genera la validacion de exito
			ejecutar($sqle,$mensaje);
			insertar_detalle_empleado ($tipnit_emp,$nit_emp,$fecreg_reemp,$tip_regemp);
		}
		else
		{
			//imprimimos un mensaje con el script que le dara la alerta al usuario de que el empleado que esta tratando de registar ya esta en la base de datos
			echo "<script>
			alert('Este empleado ya esta registrado');
			window.location= 'agreemp.php'
			</script>";
		}
		
	}
	//funcion para la creacion del correo y verificacion de disponibilidad
	function correo ($pape_emp,$pnom_emp,$pias_emp)
	{
		//variable utilizada para asignar el id del correo 
		$contador =0;
		//variable para el for para terminar la secuensia cuando se identifique la disponibilidad del corrreo
		$secuencia =0;
		for (;$secuencia <= 1; $secuencia++)
		{
			//limpiar los espacios que tenga el apellido
			$pape_emp_sinespc =str_replace(' ', '', $pape_emp);
			$pnom_emp_sinespc =str_replace(' ', '', $pnom_emp);					
			
			//variable que asigna el id para el correo
			$contador++;
			//variable para crear el correo 
			$correo = $pnom_emp_sinespc.'.'.$pape_emp_sinespc.'.'.$contador.'.'.'@cidenet.com.'.$pias_emp;
			//consulta para verificar la existencia del correo 
			$sql = "SELECT EXISTS (select * from empleado where cor_emp ='".$correo."');";
			//llamamos a la consulta que esta en la capa de conexion
			$resultado =consulta($sql);
			$row=mysqli_fetch_row($resultado);
			//if para verificar la disponibilidad del correo
			if ($row[0]=="1") 
			{    
				//reinicio la secuencia para buscar un id de correo disponible           
				$secuencia = 0;
			}
			else
			{
				//termina la accion de busqueda de disponibilidad de correo
				$secuencia = 1;
				//retorna el correo disponible
				return $correo;		
				
			}   
		}		
	}
	//funcion para verificar si es un nuevo empleado 
	function veri_emp($tipnit_emp,$nit_emp) 
	{
		//Codigo de consulta para MySQL para verificar si es nuevo el empleado
		$sql = "SELECT EXISTS (select * from empleado where tipnit_emp ='".$tipnit_emp."' and  nit_emp = '".$nit_emp."');";	
		//llamamos la funcion que va a relizar la consulta y capta el resultado
		$resultado =consulta($sql);
		$row=mysqli_fetch_row($resultado);
		//Se verifica el resultado de la consulta si hay datos o no 
		if ($row[0]=="1") 
		{
			//retorna 0 si hay datos
			return 0;
		}
		else
		{
			//retorna 1 si no hay datos 
			return 1;
		}
	}
	//En esta funcion vamos a insertar en la base de datos un registro de la hora que se llevo a cabo ya sea el registro nuevo o la modificacion de el empleado
	function insertar_detalle_empleado ($tipnit_emp,$nit_emp,$fecreg_reemp, $tip_regemp)
	{
		//codigo para MySQL para la consulta para traer el codigo del empleado para realizar el registro del detalle
		$sql = "select id_emp from empleado where tipnit_emp ='".$tipnit_emp."' and  nit_emp = '".$nit_emp."'";
		//llamar a la funcion consulta y captando el resultado
		$resultado =consulta($sql);
		//asignado a la variable row los resultados
		$row=mysqli_fetch_row($resultado);
		//condigo para insertando en la base de datos el detalle
		$sqle = "insert into regempleado ( fecreg_regemp , tip_regemp , id_emp ) values ('".$fecreg_reemp."','".$tip_regemp ."',".$row[0].")";
		
		//llamar a la funcion en la que se va a ejecutar la insertacion del detalle a la base de datos
		ejecutar($sqle,". Se a hecho la validacion con exito");	
	}
	include'../visual/convertidor_de_simbolos.php';
	//funcion de consulta sin condicional
	function con_emp()
	{ 
		//Asignamos a la variable sql el codigo a ejecutar en MySQL para consultar todas las filas almacenadas
		$sql = "select pnom_emp,
		snom_emp,
		pape_emp,
		sape_emp,
		tipnit_emp,
		nit_emp,
		pais_emp,
		cor_emp,
		est_emp,
		id_emp
		from empleado";
		
		//Llamamos a la funcion consulta que esta almacenada en nuestra conexion 
		$resultado = consulta($sql);
		//Retornamos la tabla que se ha generado en la consulta
		return $resultado;
	}
	//funcion de consulta con condicion
	function conava_emp($condicion)
	{ 
		//Asignamos a la variable sql el codigo a ejecutar en MySQL para consultar todas las filas almacenadas con la condicion que recibimos desde el formulario
		$sql = "select pnom_emp,
		snom_emp,
		pape_emp,
		sape_emp,
		tipnit_emp,
		nit_emp,
		pais_emp,
		cor_emp,
		est_emp,
		id_emp
		from empleado where $condicion";
		//tabla($sql);
		$resultado = consulta($sql);
		//Retornamos la tabla que se ha generado en la consulta
		return $resultado;
	}
	//consulta de detalle de empleado
	function detemp($id_emp)
	{
		$sql = "select * from regempleado where id_emp = ".$id_emp;
		$resultado = consulta($sql);
	?>
	<table class= "table" align="center" border="1" >
		<thead>
			<tr>
				<th>Fecha registrada</th>
				<th>Tipo de registro</th>			
			</tr>
		</thead>
		<tbody>
			<?php while($row = $resultado->fetch_assoc()) {?>
				<tr	>
					<td><?php echo $row['fecreg_regemp']?></td>
					<td><?php echo $row['tip_regemp']?></td>				
				</tr>
			<?php }?>
		</tbody>
	</table>
	
	<?php
	}
	
	//consulta simple para modificar empleados
	function conmod($id)
	{
		//condicion para traer los datos del empleado a modificar
		$sql = "select * from empleado where id_emp =".$id;
		$resultado = consulta($sql);
		return $resultado;
	}
	
	
	//funcion Modificar  empleado
	function modificar_empleado ($pape_emp , $sape_emp , $pnom_emp , $snom_emp , $pais_emp , $tipnit_emp , $nit_emp , $fecing_emp , $area_emp , $est_emp , $fecreg_reemp , $id)
	{	
		//consultamos los datos existentes
		$resultado = conmod($id);	
		//asignamos a la variable row el resultado de la consulta, el va a tomar la fila consultada
		$row = $resultado->fetch_assoc();
		//condicional si el primer apellido o primer nombre son modificados para realizar de nuevo el correo o dejar el mismo correo
		if ($pape_emp == $row['pape_emp'] && $pnom_emp == $row['pnom_emp'])
		{
			//Aqui asignamos el varlos de que esta almacenando la variable row en el campo cor_emp a la variable cor_emp que va a ser el correo que se ha generado para este empleado
			$cor_emp = $row['cor_emp'];
			//Asignamos a la variable mensaje, el mensaje que vamos a mostrar al usuario por medio de un script el cual lleva el correo del empleado que acaba de modificar y que aun seguira siendo el mismo correo y muestra el correo que tiene asignado
			$mensaje =" Su correo sigue siendo: ".$cor_emp ;
		}
		else
		{
			//Aqui vamos a modificar el correo del empleado por que se han modificado ya sea el primer nombre o primer apellido para eso llamamos a la funcion correo que nos va retornar el correo nuevo para este usuario
			$cor_emp = correo ($pape_emp,$pnom_emp,$pais_emp);
			//Vamos a asignar a la variable mensaje, el mensaje que vamos a mostrar al al usuario con el nuevo correo del empleado
			$mensaje =" Su correo ahora es: ".$cor_emp ;
		}
		//Asignamos a la variable SQL el codigo para actualizar el empleado
		//Autor del Esta pagina Israel Antonio Castiblanco Castiblanco 6-01-2021
		$sql = "UPDATE empleado
		SET pape_emp = '".$pape_emp."',
		sape_emp = '".$sape_emp."',
		pnom_emp = '".$pnom_emp."',
		snom_emp = '".$snom_emp."',
		pais_emp = '".$pais_emp."',
		tipnit_emp = '".$tipnit_emp."',
		nit_emp = '".$nit_emp."',
		fecing_emp = '".$fecing_emp."',
		area_emp = '".$area_emp."',
		est_emp = '".$est_emp."',
		cor_emp = '".$cor_emp."'
		WHERE id_emp =".$id;
		//Llamamos ala funcion ejecutar que esta en la conexion para realizar la modificacion y mostrar el mensaje 
		ejecutar($sql, $mensaje);
		//Asignamos a la variable tip_regemp el tipo de registro que se acaba de realizar en este caso es modificacion
		$tip_regemp = "Modificacion"; 
		//llamamos a la funcion insertar_detalle_empleado_modificado para realizar el registro de la base de datos de la modificacion del empleado
		insertar_detalle_empleado_modificado ($tipnit_emp,$nit_emp,$fecreg_reemp,$tip_regemp);
	}
	//funcion para registrar la fecha de modificacion del empleado
	function insertar_detalle_empleado_modificado ($tipnit_emp,$nit_emp,$fecreg_reemp, $tip_regemp)
	{
		//codigo para MySQL para la consulta para traer el codigo del empleado para realizar el registro del detalle
		$sql = "select id_emp from empleado where tipnit_emp ='".$tipnit_emp."' and  nit_emp = '".$nit_emp."'";
		//llamar a la funcion consulta y captando el resultado
		$resultado =consulta($sql);
		//asignado a la variable row los resultados
		$row=mysqli_fetch_row($resultado);
		//condigo para insertando en la base de datos el detalle
		$sqle = "insert into regempleado ( fecreg_regemp , tip_regemp , id_emp ) values ('".$fecreg_reemp."','".$tip_regemp ."',".$row[0].")";
		
		//llamar a la funcion en la que se va a ejecutar la insertacion del detalle a la base de datos
		ejecutar_redireccion($sqle,". Se a hecho la validacion con exito","busemp.php?id=1");	
	}
	
	//funcion para eliminar un empleado	
	function eliemp ($id_emp)
	{	
		//Asignamos a la variable sql el codigo para eliminar los registros del empleado las veces que ha sido modificado y cuando fue agragado 
		$sql = "DELETE FROM regempleado where id_emp = ".$id_emp;	
		//Primero borramos los registros que hay del empleado llamando a la funcion ejecutar 
		ejecutar($sql,' Se han borrado todos los registros del empleado');
		//Asignamos a la variable SQL el codigo para eliminar el empleado
		$sql = "DELETE FROM empleado where id_emp = ".$id_emp;
		//Asignamos a la variable pagina la url de donde queremos redireccionar al usuario
		$pagina = "busemp.php?id=1";
		//Lamamos la funcion ejecutar_redireccion que nos permite ejecutar el codigo sql, enviar un mensaje por script y tambien redireccionar esta funcion esta guardada en la conexion
		ejecutar_redireccion($sql,' Se ha borrado el empleado con exito',$pagina );
	}
//Autor: Israel Antonio Castiblanco Castiblanco 6-01-2021
?>

