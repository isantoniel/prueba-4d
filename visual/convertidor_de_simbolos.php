<?php
	//Esta funcion convierte los simbolos como cc a Cedula de Ciudadania
	function converti ($codigo)
	{	
		$valor;
		//Condicional de Codigo que asignara a la variable el valor segun su significado
		if ($codigo == 'co')
		{
			$valor = "Colombia";
		}
		elseif ($codigo == 'us')
		{
			$valor = 'Estados unidos';
		}
		elseif ($codigo == 'CC')
		{
			$valor = 'Cedula de Ciudadania';
		}
		elseif ($codigo == 'CE')
		{
			$valor = 'Cedula de Extranjeria';
		}
		elseif ($codigo == 'P')
		{
			$valor = 'Pasaporte';
		}
		elseif ($codigo == 'PE')
		{
			$valor = 'Permiso Especial';
		}
		else 
		{			
			$valor = $codigo;	
		}
		//Aqui nos va a retornar el valor del campon
		return $valor;
	}
//Autor: Israel Antonio Castiblanco Castiblanco 6-01-2021
?>