<?php
	
	function conectar ()
	{
		$mysqli = new mysqli("localhost","root" , "", "cidenet");
		if ($mysqli->connect_errno) 
		{
			echo "<script>
			alert('Fallo al conectar a MySQL: (".$mysqli->connect_errno.")".$mysqli->connect_error."'); 
			</script>";     	
		}	 
		return($mysqli);
	}
	function consulta ($sql)
	{
		$mysqli = conectar();
		$resultado = $mysqli->query($sql);
		if (!$resultado) 
		{		
			echo "<script>
			alert('Error de BD, no se pudo consultar la base de datos\n');               
			</script>";    	    	
			exit;
		}
		return($resultado);
	}
	
	function ejecutar_redireccion($sql,$mensaje,$href)
	{
		$db = conectar();
		$stmt = $db->stmt_init();   
		
		
		// Crear una declaración 
		$stmt = $db->prepare($sql);
		
		// Añadimos una condicional para la insercion de registros	
		if ($db->query($sql) === TRUE) {
			echo "<script>
			alert('Sea realizado con exito el procedimiento ".$mensaje."');
			window.location= '".$href."'
			</script>";
			} else {
			echo "<script>
			alert('Error:". $sql ." ".$db->error."');               
			</script>";
			
		}
		
		
		
		// Reestructurando al mensaje de error
		if(! empty( $db->error ) ){
			echo "<script>
			alert('Error:". $db->error."');               
			</script>"; // funcion para llamar al error
			
		}
	}
	
	function ejecutar ($sql,$mensaje)
	{
		
		$db = conectar();
		$stmt = $db->stmt_init();   
		
		
		// Crear una declaración 
		$stmt = $db->prepare($sql);
		
		// Añadimos una condicional para la insercion de registros	
		if ($db->query($sql) === TRUE) {			
			echo "<script>
			alert('Sea realizado con exito el procedimiento ".$mensaje."');               
			</script>";
			} else {
			echo "<script>
			alert('Error:". $sql ." ".$db->error."');               
			</script>";
			
		}
		
		
		
		// Reestructurando al mensaje de error
		if(! empty( $db->error ) ){
			echo "<script>
			alert('Error:". $db->error."');               
			</script>"; // funcion para llamar al error
			
		}
	}
?>

