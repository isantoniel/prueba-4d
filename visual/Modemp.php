<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Documento sin título</title>
	</head>
	<body>
		<?php 
			//Captamos el id del empleado a modificar
			$id = $_GET['id'];	
			
			//Se incluye la capa de funciones y busquedas de empleados
			include'../datos/Empleados.php';
			//llamamos la funcion consultar modificar que nos retornara la consulta	
			$resultado = conmod($id);	
			//Se asigna a la variable row los valores de la consulta que son los datos del empleado
			$row = $resultado->fetch_assoc();
			
		?>
		<!-- script para hacer el bloqueo de caracteres en las cajas de texto -->
		<script>
			function soloLetras(e) {
				var key = e.keyCode || e.which,
				tecla = String.fromCharCode(key).toLowerCase(),
				letras = " asdfghjklzxcvbnmqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM",
				tecla_especial = false
				
				if (letras.indexOf(tecla) == -1 ) {
					return false;
				}
			}
		</script>
		<!-- script para hacer el bloqueo de caracteres en las cajas de texto permite numeros y letras-->
		<script>
			function soloLetrasNumero(e) {
				var key = e.keyCode || e.which,
				tecla = String.fromCharCode(key).toLowerCase(),
				letras = " asdfghjklzxcvbnmqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM1234567890-",
				tecla_especial = false
				
				if (letras.indexOf(tecla) == -1 ) {
					return false;
				}
			}
		</script>
		<!--Creacion del formulario en el cual estan alojados todas las cajas de texto, combo de texto o listas, boton y hiper vicunto para regresar-->
		<form id="form1" name="form1" method="post">
			<!-- Creacion de tabla para tener un orden con las cajas de textos, botones y texto que se van a visualizar en la pagina -->
			<table border="0" align="center">
				<tbody>
					<tr>
						<td><table width="" border="1" align="center">
							<tbody>
								<tr>
									<!-- Creacion de cada una de las cajas de texto con el value imprimiedo por medio de codigo php para que muestre el datos actual del empleado -->
									<td><p>Primer apellido</p></td>
									<td><input name="pape_emp" type="text" id="textfield" value="<?php echo $row['pape_emp']?>" onkeypress="return soloLetras(event)"  pattern="[A-Z ]+" maxlength="20" ></td>
								</tr>
								<tr>
									<td><p>Segundo apellido</p></td>
									<td><input name="sape_emp" type="text" id="textfield2"  value="<?php echo $row['sape_emp']?>" maxlength="20" onkeypress="return soloLetras(event)" pattern="[A-Z ]+"></td>
								</tr>
								<tr>
									<td><p>Primer nombre</p></td>
									<td><input name="pnom_emp" type="text" id="textfield3"  value="<?php echo $row['pnom_emp']?>"   maxlength="20" onkeypress="return soloLetras(event)" pattern="[A-Z ]+"></td>
								</tr>
								<tr>
									<td><p>Otros nombre</p></td>
									<td><input name="snom_emp" type="text" id="textfield4"  value="<?php echo $row['snom_emp']?>"   maxlength="50" onkeypress="return soloLetras(event)" pattern="[A-Z ]+"></td>
								</tr>
								<tr>
									<!--Creacion de un listado o combo de texto para que el usuario seleccione el pais al que pertenece el emplado y la primera seleccion modificada con el dato actual que posee el empleado -->
									<td><p>Pais</p></td>
									<td><select name="pais_emp" title="">
										<option value="<?php echo $row['pais_emp'];?>"><?php echo converti($row['pais_emp'])?></option>
										<option value="co">Colombia</option>
										<option value="us">Estados unidos</option>
									</select></td>
								</tr>
								<tr>
									<td><p>Tipo de documento</p></td>
									<td><p>
										<select name="tipnit_emp" title="">
											<option value="<?php echo $row['tipnit_emp']?>"><?php echo converti($row['tipnit_emp'])?></option>
											<option value="CC">Cédula de Ciudadanía</option>
											<option value="CE">Cédula de Extranjería</option>
											<option value="P">Pasaporte</option>
											<option value="PE">Permiso Especial</option>
										</select>
										<br>
									</p></td>
								</tr>
								<tr>
									<td><p>Número de Identificación</p></td>
									<td><input name="nit_emp" type="text" id="textfield5" value="<?php echo $row['nit_emp']?>"  maxlength="20" onkeypress="return soloLetrasNumero(event)" pattern="[A-Z-0-9]+"></td>
								</tr>
								<tr>
									<!-- Creacion  de un date que va a permitir por medio de codigo php un rango minino que es igual a la fecha actual de hoy menos un mes y como maximo la fecha actual de hoy y con el value modificada con la fecha en la cual ingreso el empleado-->
									<td><p>Fecha de ingreso</p></td>
									<td><input name= "fecing_emp" type="date" value="<?php  echo date("Y-m-d",strtotime($row['fecing_emp']))?>" min="<?php
										//Variable para captar la fecha de hoy
										$fecha_actual = date("d-m-Y");
										//se imprime la fecha minima que es del dia actual menos un mes para que el date lo tome como fecha minima que se va a tomar
										echo date("Y-m-d", strtotime($fecha_actual."- 1 month"))
										?>" max="<?php
										//Se imprime en pantalla la fecha de hoy para que el date solo permita esta fecha como maxima
										echo date("Y-m-d")
									?>"
									></td>
								</tr>
								<tr>
									<td><p>Area</p></td>
									<td><select name="area_emp" title="">
										<option value="<?php echo $row['area_emp']?>"><?php echo $row['area_emp'] ?></option>
										<option value="Administracion">Administración</option>
										<option value="Financiera">Financiera</option>
										<option value="Compras">Compras</option>
										<option value="Operacion">Operación</option>
										<option value="Talento Humano">Talento Humano</option>
										<option value="Servicios Varios">Servicios Varios</option>
									</select></td>
								</tr>
								<tr>
									<!-- Aqui se muestra el estado actual de empleado y se puede modificar a inactivo-->
									<td><p>Estado</p></td>
									<td><select name="est_emp" title="">
										<option value="<?php echo $row['est_emp']?>"><?php echo $row['est_emp'] ?></option>
										<option value="ACTIVO">ACTIVO</option>
										<option value="INACTIVO">INACTIVO</option>
									</select></td>
								</tr>
								<tr>
									<!-- Aqui se va a generar la fecha de modificacion del empleado que va a ser guardada en la base de datos-->
									<td><p>Fecha de modificacion</p></td>
									<td><?php 
										date_default_timezone_set('America/Bogota');
									echo date("Y-m-d H:i:s");?></td>
								</tr>
								<tr>
									<td><p>
										<!--Creacion del boton modificar que va permitir realizar el proceso de modificacion de los datos del empleado -->
										<input type="submit" name="agremp" id="submit" value="Modificar empleado">
									</p></td>
									<td><input type="reset" name="reset" id="reset" value="Restablecer">
									<a href="busemp.php?id=1"> Regresar</a></td>
								</tr>
							</tbody>
						</table></td>
						<td><?php detemp($id) ?></td>
					</tr>
				</tbody>
			</table>
			
			<?php
				
				//Modificar empleado
				if (isset($_REQUEST['agremp']))
				{
					//asignacion de variables para cada campo
					$pape_emp = $_REQUEST['pape_emp'];
					$sape_emp = $_REQUEST['sape_emp'];
					$pnom_emp = $_REQUEST['pnom_emp'];
					$snom_emp = $_REQUEST['snom_emp'];
					$pais_emp = $_REQUEST['pais_emp'];
					$tipnit_emp = $_REQUEST['tipnit_emp'];
					$nit_emp = $_REQUEST['nit_emp'];
					$fecing_emp =  $_REQUEST['fecing_emp'];
					$area_emp = $_REQUEST['area_emp'];
					$est_emp = $_REQUEST['est_emp'];
					$fecreg_reemp = date("Y-m-d H:i:s");
					//validacion que ningun campo este en blanco o no haya dejado sin seleccionar un listado
					if (trim($pape_emp) == '' || trim($sape_emp) == '' || trim($pnom_emp) == '' || trim($pais_emp) == '' || trim($tipnit_emp) == '' || trim($nit_emp) == '' || trim($snom_emp) == '' || trim($fecing_emp ) == '' || $area_emp == '' )
					{		
						//alerta de que debe llenar todos los campos por medio de script
						echo "<script>
						alert('Todos los campos son necesarios para el registro');
						window.location= 'agreemp.php'
						</script>";
					}
					else
					{	
						//Se llama a la funcion modificar_empleado y se envian todos los datos previamente asignados en variables para realizar el cambio en la base de datos
						modificar_empleado($pape_emp,$sape_emp,$pnom_emp,$snom_emp,$pais_emp,$tipnit_emp,$nit_emp,$fecing_emp,$area_emp,$est_emp,$fecreg_reemp,$id);
						
					}
				}
			?>
		</form>
		<!-- Autor: Israel Antonio Castiblanco Castiblanco 6-01-2021-->
	</body>
</html>
